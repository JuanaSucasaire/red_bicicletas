const tilesProvider = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

let myMap = L.map('myMap').setView([-34.652115, -58.387585], 13);

L.tileLayer(tilesProvider, {
    maxZoom: 18,
}).addTo(myMap);

//let marker = L.marker([-34.652115, -58.387585]).addTo(myMap);
/*
$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            let marker = L.marker(bici.ubicacion, {title: bici.id}).addTo(myMap);
        });
    }
})*/