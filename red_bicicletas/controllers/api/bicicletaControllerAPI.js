let Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res) {
    Bicicleta.find({}, function(err, bicicletas) {
        res.status(200).json({
            bicis: bicicletas
        });
    });
}

exports.bicicleta_create = function(req, res){  
    let ubicacion = [req.body.lat, req.body.lng];
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, ubicacion);
    
    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    });  
}

exports.bicicleta_update = function(req, res) {
    const bici = Bicicleta.findByCode(req.params.code);
    id = bici._id;
    /*
    bici.code = req.body.code;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];*/

    Bicicleta.findByIdAndUpdate(id, req.body , {new: true});
    
    res.status(200).json({
        resultado: "Se actualizaron correctamente los datos"
    });
}

exports.bicicleta_delete = function(req, res) {
    Bicicleta.removeByCode(req.body.code, function(err, result) {
        if(err) {
            res.status(400).json({
                error: err
            })
        }
        res.status(204).send();
    });
}