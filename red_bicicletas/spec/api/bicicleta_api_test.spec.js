let mongoose = require('mongoose');
let Bicicleta = require('../../models/bicicleta');
let request = require('request');
//let server = require('../../bin/www');

describe('Bicicleta API', ()=> {

    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {
            useCreateIndex: true,
            useUnifiedTopology:true,
            useNewUrlParser: true
        });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('we are connected to test database');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if(err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
    });

    describe('GET BICICLETAS /', ()=> {
        it('Status 200', (done)=> {
            request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
                let result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', ()=> {
        it('Status 200', (done)=> {
            
            let headers = {'content-type': 'application/json'};
            let aBici = '{"code": 5,"color": "roja","modelo": "BMX","lat":-33.3453,"lng":-52.2342}';

            request.post({
                headers:headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                let bici = JSON.parse(body).bicicleta;
                expect(bici.code).toBe(5);
                expect(bici.color).toBe("roja");
                expect(bici.modelo).toBe("BMX");
                done();
            })
        });
    });
    
    describe('DELETE BICICLETAS /delete', ()=> {
        it('Status 204', (done)=> {

            let headers = {'content-type': 'application/json'};
            let aBici = '{"code": 10,"color": "roja","modelo": "BMX","lat":-33.3453,"lng":-52.2342}';

            request.post({
                headers:headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
            });

            request.delete({
                headers:headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: '{"code": 10}'
            }, function (error, response, body) {
                expect(response.statusCode).toBe(204);
                done();
            })
        });
    });
});