let mongoose = require('mongoose');
let Bicicleta = require('../../models/bicicleta');


describe('Testing Bicicletas', function() {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {
            useCreateIndex: true,
            useUnifiedTopology:true,
            useNewUrlParser: true
        });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('we are connected to test database');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if(err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
    });

    describe('Bicicleta.createInstance', ()=> {
        it('Crea una instancia de bicicleta', ()=> {
            var bici = Bicicleta.createInstance(1,"verde","urbana", [-34.5, -54.1]);
  
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        });
    });

    describe('Bicicleta.allBicis', ()=> {
        it('Comienza vacia', (done)=> {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', ()=> {
        it('agrega solo una bici', (done)=> {
            var aBici = new Bicicleta({code: 1, color: 'verde', modelo: 'urbana'});
            Bicicleta.add(aBici, function(err, newBici) {
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });

        });
    });

    describe('Bicicleta.findByCode', ()=> {
        it('debe devolver la bici con el code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color:'verde', modelo:'urbana'});
                Bicicleta.add(aBici, function(err, newBici) {
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color:'rojo', modelo:'urbana'});
                    Bicicleta.add(aBici2, function(err, newBici) {
                        if(err) console.log(err);

                        Bicicleta.findByCode(1, function(err, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', ()=> {
        it('debe eliminar una bicicleta', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color:'verde', modelo:'urbana'});
                Bicicleta.add(aBici, function(err, newBici) {
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color:'rojo', modelo:'urbana'});
                    Bicicleta.add(aBici2, function(err, newBici) {
                        if(err) console.log(err);

                        Bicicleta.removeByCode(1, function(err, targetBici) {
                            if(err) console.log(err);

                            Bicicleta.allBicis(function(err, bicis) {
                                expect(bicis.length).toBe(1);
                                expect(bicis[1]).toBe(undefined);

                                done();
                            });
                        });
                    });
                });
            });
        });
    });

});



/*
//Esto se va a ejecutar antes de cada tests
beforeEach( ()=> {Bicicleta.allBicis = [];} );

describe('Bicicleta.allBicis', ()=> {
    it('Comienza vacia', ()=> {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', ()=> {
    it('Agrega una bicicleta', ()=> {
        expect(Bicicleta.allBicis.length).toBe(0);

        let a = new Bicicleta(1, 'Azul', 'Urbana', [-34.649883, -58.389092]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', ()=> {
    it('Trae la bicicleta correcta', ()=> {
        expect(Bicicleta.allBicis.length).toBe(0);

        let a = new Bicicleta(1, 'Negro', 'Montain Bike', [-34.627348, -58.370210]);
        let b = new Bicicleta(2, 'Rojo', 'BMX', [-34.637232, -58.373711]);
        let c = new Bicicleta(3, 'Amarillo', 'Electrica', [-34.633060, -58.366573]);

        Bicicleta.add(a);
        Bicicleta.add(b);
        Bicicleta.add(c);

        let targetBici = Bicicleta.findById(2);

        expect(targetBici.id).toBe(2);
        expect(targetBici.color).toBe(b.color);
        expect(targetBici.modelo).toBe(b.modelo);
    });
});

describe('Bicicleta.removeById', ()=> {
    it('Elimina la bicicleta correcta', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);

        let a = new Bicicleta(1, 'Negro', 'Montain Bike', [-34.627348, -58.370210]);
        let b = new Bicicleta(2, 'Rojo', 'BMX', [-34.637232, -58.373711]);
        let c = new Bicicleta(3, 'Amarillo', 'Electrica', [-34.633060, -58.366573]);

        Bicicleta.add(a);
        Bicicleta.add(b);
        Bicicleta.add(c);

        Bicicleta.removeById(3);

        expect(Bicicleta.allBicis.length).toBe(2);
        expect(Bicicleta.allBicis[2]).toBe(undefined);
    });
});*/