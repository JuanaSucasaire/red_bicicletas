var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion:{type: [Number], index: { type: '2dsphere', sparse: true}}
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    })
}

bicicletaSchema.methods.toString = function() {
    return `Code: ${this.code} | Color: ${this.color} | Modelo: ${this.modelo}`
}

bicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb);
}

bicicletaSchema.statics.add = function(aBici, cb) {
    this.create(aBici, cb);
}

bicicletaSchema.statics.findByCode = function(aCode, cb) {
    return this.findOne({code: aCode}, cb);
}

bicicletaSchema.statics.removeByCode = function(aCode, cb) {
    return this.deleteOne({code: aCode}, cb);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);


/*
Bicicleta.findById = function(aBiciId) {
    let aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if(aBici) {
        return aBici;
    } else {
        throw new Error(`No existe una bicicleta con el id: ${aBiciId}`);
    }
}

Bicicleta.removeById = function(aBiciId) {
    
    for(let i = 0; i < Bicicleta.allBicis.length; i++) {
        if(Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    } 
}*/

/*
let a = new Bicicleta(1, 'Azul', 'Urbana', [-34.649883, -58.389092]);
let b = new Bicicleta(2, 'Negro', 'Montain Bike', [-34.627348, -58.370210]);
let c = new Bicicleta(3, 'Rojo', 'BMX', [-34.637232, -58.373711]);
let d = new Bicicleta(4, 'Amarillo', 'Electrica', [-34.633060, -58.366573]);
let e = new Bicicleta(5, 'Verde', 'Plegable', [-34.632747, -58.385279]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
Bicicleta.add(d);
Bicicleta.add(e);
*/